//
//  CryptoApp.swift
//  Crypto
//
//  Created by Keith Staines on 28/10/2021.
//

import SwiftUI

@main
struct CryptoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
